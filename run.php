<?php

require_once 'functions.php';
require_once 'classes' . DIRECTORY_SEPARATOR . 'Board.php';
require_once 'classes' . DIRECTORY_SEPARATOR . 'Point.php';
require_once 'classes' . DIRECTORY_SEPARATOR . 'Snake.php';
require_once 'classes' . DIRECTORY_SEPARATOR . 'SnakeBodyPart.php';
require_once 'classes' . DIRECTORY_SEPARATOR . 'Timer.php';
require_once 'classes' . DIRECTORY_SEPARATOR . 'Game.php';

$game = new Game();

$game->mainLoop();
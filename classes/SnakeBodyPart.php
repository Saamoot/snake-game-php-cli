<?php

class SnakeBodyPart
{
    private $x = -1;
    private $y = -1;
    /** @var null SnakeBodyPart */
    private $childBodyPart = null;

    public function __construct($x, $y)
    {
        $this->setX($x);
        $this->setY($y);
    }

    /**
     * @return int
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param int $x
     * @return SnakeBodyPart
     */
    public function setX($x)
    {
        $this->x = $x;
        return $this;
    }

    /**
     * @return int
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param int $y
     * @return SnakeBodyPart
     */
    public function setY($y)
    {
        $this->y = $y;
        return $this;
    }

    /**
     * @return null|SnakeBodyPart
     */
    public function getChildBodyPart()
    {
        return $this->childBodyPart;
    }

    /**
     * @param null|SnakeBodyPart $childBodyPart
     * @return SnakeBodyPart
     */
    public function setChildBodyPart($childBodyPart)
    {
        $this->childBodyPart = $childBodyPart;
        return $this;
    }

    public function getLastSnakeBodyPart()
    {
        $result = $this;

        while (true) {
            $nextBodyPart = $result->getChildBodyPart();

            if (true === is_null($nextBodyPart)) {
                break;
            }

            $result = $nextBodyPart;
        }

        return $result;
    }

    public function moveBodyParts($x, $y)
    {
        if (true === $this->hasChildBodyPart()) {
            $this->getChildBodyPart()
                ->moveBodyParts($this->getX(), $this->getY());
        }

        $this->setX($x);
        $this->setY($y);
    }

    /**
     * @return bool
     */
    public function hasChildBodyPart()
    {
        return false === is_null($this->getChildBodyPart());
    }

}
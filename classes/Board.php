<?php

class Board
{
    private $width = 40;
    private $height = 10;
    private $wallChar = '#';
    private $snakeChar = '*';
    private $snakeFirstChar = '@';
    private $pointCHar = '+';
    /** @var Point[] */
    private $points = [];


    /**
     * @var null|Snake
     */
    private $snake = null;

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    public function getHalfWidth()
    {
        return floor($this->getWidth() / 2);
    }

    public function outsideBoard($x, $y)
    {
        $halfWidth = $this->getHalfWidth();
        $halfHeight = $this->getHalfHeight();


        if (-$halfWidth >= $x || $halfWidth <= $x) {
            return true;
        }

        if (-$halfHeight >= $y || $halfHeight <= $y) {
            return true;
        }

        return false;
    }

    /**
     * @param int $width
     * @return Board
     */
    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    public function getHalfHeight()
    {
        return floor($this->getHeight() / 2);
    }

    /**
     * @param int $height
     * @return Board
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return string
     */
    public function getWallChar()
    {
        return $this->wallChar;
    }

    /**
     * @param string $wallChar
     * @return Board
     */
    public function setWallChar($wallChar)
    {
        $this->wallChar = $wallChar;
        return $this;
    }

    /**
     * @return Snake|null
     */
    public function getSnake()
    {
        return $this->snake;
    }

    /**
     * @param Snake|null $snake
     * @return Board
     */
    public function setSnake($snake)
    {
        $this->snake = $snake;
        return $this;
    }

    public function addPoint($point)
    {
        $this->points[] = $point;

        return $this;
    }

    public function getPoints()
    {
        return $this->points;
    }

    public function getPointCount()
    {
        return count($this->getPoints());
    }

    public function removePoint($key)
    {
        unset($this->points[$key]);
    }

    public function clearPoints()
    {
        $this->points = [];

        return $this;
    }


    public function toArray()
    {
        $halfWidth = $this->getHalfWidth();
        $halfHeight = $this->getHalfHeight();

        $result = [];
        for ($i = -$halfWidth; $i < $halfWidth; $i++) {
            for ($j = -$halfHeight; $j < $halfHeight; $j++) {
                $result[$j][$i] = ' ';
            }
        }

        for ($i = -$halfWidth; $i < $halfWidth; $i++) {
            $result[-$halfHeight][$i] = $this->wallChar;
            $result[$halfHeight][$i] = $this->wallChar;
        }

        for ($i = -$halfHeight; $i < $halfHeight; $i++) {
            $result[$i][-$halfWidth] = $this->wallChar;
            $result[$i][$halfWidth] = $this->wallChar;
        }

        foreach ($this->points as $point) {
            $result[$point->getY()][$point->getX()] = $this->pointCHar;
        }

        if (false === is_null($this->getSnake())) {
            $snakeBodyParty = $this->getSnake()->getHeadSnakeBodyPart();
            $char = $this->snakeFirstChar;
            while (false === is_null($snakeBodyParty)) {
                $result[$snakeBodyParty->getY()][$snakeBodyParty->getX()] = $char;
                $char = $this->snakeChar;

                $snakeBodyParty = $snakeBodyParty->getChildBodyPart();
            }
        }

        return $result;
    }

    public function toString()
    {
        $mapArray = $this->toArray();
        $result = '';
        foreach ($mapArray as $row => $columns) {
            $result .= implode('', $columns) . PHP_EOL;
        }

        return $result;
    }
}
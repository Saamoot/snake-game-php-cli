<?php

class Timer
{
    private $active = false;
    private $intervalMilliSeconds = 250;
    private $callbacks = [];

    public function clearCallbacks()
    {
        $this->callbacks = [];

        return $this;
    }

    public function addCallback($callback, $index = null)
    {
        if (true === is_null($index)) {
            $index = count($this->callbacks);
        }

        $this->callbacks[$index] = $callback;

        return $this;
    }

    public function getCallbacksKeys()
    {
        return array_keys($this->callbacks);
    }


    public function tick()
    {
        foreach ($this->callbacks as $key => $callback) {
            $callback();
        }

        return $this;
    }

    public function run()
    {
        $this->active = true;
        while (true === $this->active) {
            $this->tick();

            usleep($this->intervalMilliSeconds * 1000);
        }

        return $this;
    }

    public function stop()
    {
        $this->active = false;

        return $this;
    }
}
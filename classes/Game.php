<?php

class Game
{
    public $points = 0;
    /** @var null|Snake */
    public $snake = null;
    /** @var Board */
    public $board = null;
    /** @var null|\Timer */
    public $timer        = null;
    public $isMenuScreen = true;
    public $isPlaying    = false;
    public $closeGame    = false;
    public $stdin        = null;
    public $lastChar     = '';

    public function __construct()
    {
        $this->board = new Board();
        $this->setNonInterruptingReadInput();
        $this->reset();
    }

    public function setNonInterruptingReadInput()
    {
        system('stty cbreak -echo');
        $this->stdin = fopen('php://stdin', 'r');
        stream_set_blocking($this->stdin, 0);
    }

    public function reset()
    {
        $this->points = 0;
        $this->board->setSnake(null);
        $this->snake = new Snake(0, 0, 1, 0);
        $this->timer = new Timer();
        $this->board->setSnake($this->snake);
        $this->board->clearPoints();
    }

    public function startGame()
    {
        $this->isPlaying    = true;
        $this->isMenuScreen = false;
        $this->timer->run();
    }

    public function readInput()
    {
        $this->timer->addCallback(function () {
            $this->lastChar = $this->getInputChar();
        });
    }

    public function gameControlInput()
    {
        $this->timer->addCallback(function () {
            if (32 == $this->lastChar) { // space
                $this->reset();
                $this->startGame();
                $this->lastChar = '';
            }
            if (113 == $this->lastChar) { // q
                $this->stopGame();
                $this->lastChar = '';
            }
        });
    }

    public function stopGame()
    {
        $this->timer->stop();
        $this->closeGame    = true;
        $this->isPlaying    = false;
        $this->isMenuScreen = false;
    }

    public function snakeControlInput()
    {
        $this->timer->addCallback(function () {
            if (true === $this->snake->inputKey($this->lastChar)) {
                $this->lastChar = '';
            }
        });
    }

    /**
     * @return bool
     */
    public function collisionSnakeWithBoard()
    {
        $snakeBodyPart = $this->snake->getHeadSnakeBodyPart();

        return true === $this->board->outsideBoard($snakeBodyPart->getX(), $snakeBodyPart->getY());
    }

    public function collisionSnakeWithPoint()
    {
        $snakeBodyPart = $this->snake->getHeadSnakeBodyPart();

        foreach ($this->board->getPoints() as $key => $point) {
            if ($point->getX() === $snakeBodyPart->getX() && $point->getY() === $snakeBodyPart->getY()) {
                $this->points += $point->getValue();
                $this->board->removePoint($key);

                $snakeBodyPart
                    ->getLastSnakeBodyPart()
                    ->setChildBodyPart(new SnakeBodyPart($point->getX(), $point->getY()));

                break;
            }
        }
    }

    /**
     * @return bool
     */
    public function collisionSnakeWithSnake()
    {
        $snakeBodyParts    = $this->snake->getSnakeBodyParts();
        $snakeHeadBodyPart = $this->snake->getHeadSnakeBodyPart();
        array_shift($snakeBodyParts);

        foreach ($snakeBodyParts as $snakeBodyPart) {
            if (
                $snakeHeadBodyPart->getX() === $snakeBodyPart->getX()
                && $snakeHeadBodyPart->getY() === $snakeBodyPart->getY()
            ) {
                return true;
            }
        }

        return false;
    }

    public function mainLoop()
    {
        $this->readInput();
        $this->gameControlInput();
        $this->snakeControlInput();
        $this->screenClear();
        $this->screenUpdate();
        $this->startGame();
    }

    public function screenClear()
    {
        $this->timer->addCallback(function (){
            clearScreen();
        });
    }

    public function screenUpdate()
    {
        $this->timer->addCallback(function () {
            if (true === $this->isMenuScreen) {
                $this->menuScreen();
            } else {
                if (true === $this->isPlaying) {
                    $this->gameScreen();
                } else {
                    $this->endGameScreen();
                }
            }

            if (true === $this->closeGame) {
                $this->closeGameScreen();
            }
        });
    }

    public function menuScreen()
    {
        echo PHP_EOL;
        echo PHP_EOL;
        echo "SNAKE GAME !!";
        echo PHP_EOL;
        echo PHP_EOL;
        echo PHP_EOL;
        echo "press space to start";
        echo PHP_EOL;
        echo "press q to quit";
        echo PHP_EOL;
        echo PHP_EOL;
        echo PHP_EOL;
    }

    public function gameScreen()
    {
        echo 'Points = ' . $this->points . PHP_EOL;
        $this->spawnPoint();
        $this->snake->move();

        if (
            true === $this->collisionSnakeWithBoard()
            || true === $this->collisionSnakeWithSnake()
        ) {

            $this->isPlaying = false;

            return;
        }

        $this->collisionSnakeWithPoint();

        echo $this->board->toString();
    }

    public function endGameScreen()
    {
        echo PHP_EOL;
        echo PHP_EOL;
        echo "Game over";
        echo PHP_EOL;
        echo "Your have earned {$this->points} points !!";
        echo PHP_EOL;
        echo PHP_EOL;
        echo PHP_EOL;
        echo "press space to restart";
        echo PHP_EOL;
        echo "press q to quit";
        echo PHP_EOL;
        echo PHP_EOL;
        echo PHP_EOL;
    }

    public function closeGameScreen()
    {
        echo PHP_EOL;
        echo PHP_EOL;
        echo 'thanks for playing, bye';
        echo PHP_EOL;
        echo PHP_EOL;
    }

    public function spawnPoint()
    {
        if ($this->board->getPointCount() > 0) {
            return;
        }

        $x = rand(0, $this->board->getHalfWidth() - 1);
        if (rand(0, 1) > 0) {
            $x *= -1;
        }

        $y = rand(0, $this->board->getHalfHeight() - 1);
        if (rand(0, 1) > 0) {
            $y *= -1;
        }

        $this->board->addPoint(new Point($x, $y));
    }

    /**
     * @return int
     */
    function getInputChar()
    {
        $c = ord(fgetc($this->stdin));

        return $c;
    }

}
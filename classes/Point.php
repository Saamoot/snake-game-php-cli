<?php

class Point
{
    private $x = 0;
    private $y = 0;
    private $value = 1;

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     * @return Point
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }


    /**
     * @return int
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param int $x
     * @return Point
     */
    public function setX($x)
    {
        $this->x = $x;
        return $this;
    }

    /**
     * @return int
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param int $y
     * @return Point
     */
    public function setY($y)
    {
        $this->y = $y;
        return $this;
    }

    public function __construct($x, $y, $value = 1)
    {
        $this->setX($x);
        $this->setY($y);
        $this->setValue($value);
    }
}
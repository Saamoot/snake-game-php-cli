<?php

class Snake
{
    private $headSnakeBodyPart = null;
    private $vectorX = 0;
    private $vectorY = 0;

    public function __construct($startX, $startY, $vectorX, $vectorY)
    {
        $this->setHeadSnakeBodyPart(new SnakeBodyPart($startX, $startY));

        if ($vectorX !== 0 && $vectorY) {
            throw new Exception('Only vectorX or vectorY can be set at a time');
        }

        $this->setVectorX($vectorX);
        $this->setVectorY($vectorY);
    }

    public function init()
    {
        $snakeBodyBart = $this->getHeadSnakeBodyPart()->getLastSnakeBodyPart();
        $newSnakeBodyPart = new SnakeBodyPart($snakeBodyBart->getX(), $snakeBodyBart->getY() + 1);
        $snakeBodyBart->setChildBodyPart($newSnakeBodyPart);
        $snakeBodyBart = $newSnakeBodyPart;
        $newSnakeBodyPart = new SnakeBodyPart($snakeBodyBart->getX(), $snakeBodyBart->getY() + 1);
        $snakeBodyBart->setChildBodyPart($newSnakeBodyPart);
    }

    /**
     * @return null|SnakeBodyPart
     */
    public function getHeadSnakeBodyPart()
    {
        return $this->headSnakeBodyPart;
    }

    /**
     * @param null|SnakeBodyPart $headSnakeBodyPart
     * @return Snake
     */
    public function setHeadSnakeBodyPart($headSnakeBodyPart)
    {
        $this->headSnakeBodyPart = $headSnakeBodyPart;
        return $this;
    }

    /**
     * @return int
     */
    public function getVectorX()
    {
        return $this->vectorX;
    }

    /**
     * @param int $vectorX
     * @return Snake
     */
    public function setVectorX($vectorX)
    {
        $this->vectorX = $vectorX;
        return $this;
    }

    /**
     * @return int
     */
    public function getVectorY()
    {
        return $this->vectorY;
    }

    /**
     * @param int $vectorY
     * @return Snake
     */
    public function setVectorY($vectorY)
    {
        $this->vectorY = $vectorY;
        return $this;
    }

    public function move()
    {
        $this->getHeadSnakeBodyPart()->moveBodyParts(
            $this->getHeadSnakeBodyPart()->getX() + $this->getVectorX(),
            $this->getHeadSnakeBodyPart()->getY() + $this->getVectorY()
        );
    }

    public function inputKey($key)
    {
        $keyUsed = false;
        switch ($key) {
//            case 119: #up/w
//            break;
//            case 115: #down/s
//            break;
            case 97: #left/a
                $keyUsed = true;
                if (0 !== $this->vectorX) {
                    if ($this->vectorX > 0) {
                        $this->vectorX = 0;
                        $this->vectorY = -1;
                    } else {
                        $this->vectorX = 0;
                        $this->vectorY = 1;
                    }
                } else if (0 !== $this->vectorY) {
                    if ($this->vectorY > 0) {
                        $this->vectorY = 0;
                        $this->vectorX = 1;
                    } else {
                        $this->vectorY = 0;
                        $this->vectorX = -1;
                    }
                }
                break;
            case 100: #right/d
                $keyUsed = true;
                if (0 !== $this->vectorX) {
                    if ($this->vectorX > 0) {
                        $this->vectorX = 0;
                        $this->vectorY = 1;
                    } else {
                        $this->vectorX = 0;
                        $this->vectorY = -1;
                    }
                } else if (0 !== $this->vectorY) {
                    if ($this->vectorY > 0) {
                        $this->vectorY = 0;
                        $this->vectorX = -1;
                    } else {
                        $this->vectorY = 0;
                        $this->vectorX = 1;
                    }
                }
                break;
        }

        return $keyUsed;
    }

    /**
     * @return SnakeBodyPart[]
     */
    public function getSnakeBodyParts()
    {
        $result = [];

        $snakeBodyPart = $this->getHeadSnakeBodyPart();
        $result[] = $snakeBodyPart;
        while (true === $snakeBodyPart->hasChildBodyPart()) {
            $snakeBodyPart = $snakeBodyPart->getChildBodyPart();
            $result[] = $snakeBodyPart;
        }

        return $result;
    }


}